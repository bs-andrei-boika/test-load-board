﻿using System.Net;
using System.Web.Http;
using System.Web.Http.Results;

namespace Web.Api.Controllers
{
    public sealed class ErrorController : ApiController
    {
        [HttpGet, HttpPost, HttpPut, HttpDelete, HttpHead, HttpOptions, AcceptVerbs("PATCH")]
        public IHttpActionResult Handle404()
        {
            var error = new { Message = "The requested resource is not found." };
            return new NegotiatedContentResult<object>(HttpStatusCode.NotFound, error, this);
        }
    }
}