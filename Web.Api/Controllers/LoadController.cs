﻿using System.Web.Http;
using Web.Api.Models.Load;
using Web.Api.Security;
using Web.Api.Tools;

namespace Web.Api.Controllers
{
    [RoutePrefix("api/v1.0/loads")]
    public sealed class LoadController : BaseController
    {


        public LoadController()
        {
        }

        [Route]
        [HttpPost]
        [Validation]
        [Authorize(Roles = Roles.LoadOwner)]
        public IHttpActionResult Place(PlaceLoadModel model)
        {
            return Ok();
        }


        [Route]
        [HttpGet]
        [Authorize(Roles = Roles.LoadOwner)]
        public IHttpActionResult Get()
        {
            return Ok();
        }


        [Route("check")]
        [HttpPost]
        [Validation]
        [Authorize(Roles = Roles.Carrier)]
        public IHttpActionResult Check()
        {
            return Ok();
        }
    }
}