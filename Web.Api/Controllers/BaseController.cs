﻿using System.Net;
using System.Web.Http;
using System.Web.Http.Results;

namespace Web.Api.Controllers
{
    public abstract class BaseController : ApiController
    {
        protected IHttpActionResult BadRequest<T>(T content)
        {
            return new NegotiatedContentResult<T>(HttpStatusCode.BadRequest, content, this);
        }

        protected IHttpActionResult Forbidden<T>(T content)
        {
            return new NegotiatedContentResult<T>(HttpStatusCode.Forbidden, content, this);
        }
    }
}