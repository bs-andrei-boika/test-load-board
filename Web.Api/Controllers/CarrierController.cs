﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using DataAccess.Model.Data;
using Web.Api.Models.User;
using Web.Api.Security;

namespace Web.Api.Controllers
{
    [RoutePrefix("api/v1.0/carriers")]
    public class CarrierController : BaseController
    {
        private readonly ApplicationDbContext _dbContext;

        public CarrierController(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [Route]
        [HttpGet]
        //[Authorize(Roles = Roles.LoadOwner)]
        public async Task<IHttpActionResult> GetCarriers()
        {
            var carrierRole = await _dbContext.Roles.FirstOrDefaultAsync(r => r.Name == AccountType.Carrier.ToString());
            if (carrierRole != null)
            {
                var carriers = await _dbContext.Users.Include(u => u.Roles).Where(u => u.Roles.Any(r => r.RoleId == carrierRole.Id)).ToListAsync();
                return Ok(carriers);
            }
            return BadRequest("");
        }
    }
}