﻿using System.Web.Http;

namespace Web.Api.Controllers
{
    [RoutePrefix("api/v1.0/ping")]
    public sealed class PingController : ApiController
    {
        [Route]
        [HttpGet]
        public IHttpActionResult Get()
        {
            return Ok("OK");
        }
   }
}