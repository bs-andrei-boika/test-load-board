﻿using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Mvc;
using DataAccess.Model.Data;
using Web.Api.Managers;
using Web.Api.Models;
using Web.Api.Models.User;
using Web.Api.Security;
using Web.Api.Tools;

namespace Web.Api.Controllers
{
    [System.Web.Http.RoutePrefix("api/v1.0/account")]
    public sealed class AccountController : BaseController
    {
        private readonly ApplicationUserManager _userManager;

        public AccountController(ApplicationUserManager userManager)
        {
            _userManager = userManager;
        }

        [System.Web.Http.Route]
        [System.Web.Http.HttpPost]
        [Validation]
        public async Task<IHttpActionResult> Register(RegisterModel model)
        {
            var user = new ApplicationUser
            {
                Email = model.Email,
                FullName = model.FullName
            };

            var result = await _userManager.CreateAsync(user, model.Password);

            if (result.Succeeded)
            {
                await _userManager.AddToRoleAsync(user.Id, model.AccountType.ToString());

                var code = await _userManager.GenerateEmailConfirmationTokenAsync(user.Id);
                var callbackUrl = this.Url.Link("Default", new { Controller = "Account", Action = "Verify", userId = user.Id, code = code });
                await _userManager.SendEmailAsync(user.Id,
                   "Confirm your account",
                   "Please confirm your account by clicking this link: <a href=\""
                                                   + callbackUrl + "\">link</a>");

                //await _signInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            }
            else
            {
                return BadRequest(new ErrorModel("Error"));
            }

            return Ok();
        }


        [System.Web.Http.Route("verify")]
        [System.Web.Http.HttpPost]
        [Validation]
        public async Task<IHttpActionResult> Verify(VerifyEmailModel model)
        {
            if (model.UserId == null || model.Code == null)
            {
                return BadRequest(new ErrorModel("Error"));
            }
            var result = await _userManager.ConfirmEmailAsync(model.UserId, model.Code);
            return result.Succeeded ? Ok() : BadRequest(new ErrorModel("Error"));
        }

    }
}