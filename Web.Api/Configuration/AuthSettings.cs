﻿using System.Configuration;

namespace Web.Api.Configuration
{
    public sealed class AuthSettings : ConfigurationSection
    {
        [ConfigurationProperty("expirationMinutes", IsRequired = true)]
        public int AccessTokenExpirationMinutes
        {
            get { return (int)this["expirationMinutes"]; }
            set { this["expirationMinutes"] = value; }
        }

        [ConfigurationProperty("jwtKey", IsRequired = true)]
        public string JwtKey
        {
            get { return (string)this["jwtKey"]; }
            set { this["jwtKey"] = value; }
        }

        [ConfigurationProperty("jwtIssuer")]
        public string JwtIssuer
        {
            get { return (string)this["jwtIssuer"]; }
            set { this["jwtIssuer"] = value; }
        }

        [ConfigurationProperty("jwtAudience", DefaultValue = "all")]
        public string JwtAudience
        {
            get { return (string)this["jwtAudience"]; }
            set { this["jwtAudience"] = value; }
        }

        [ConfigurationProperty("httpsRequired", DefaultValue = "true")]
        public bool HttpsRequired
        {
            get { return (bool)this["httpsRequired"]; }
            set { this["httpsRequired"] = value; }
        }
    }
}