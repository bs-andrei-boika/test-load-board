﻿using System.Configuration;
using System.Reflection;
using DataAccess.Model.Data;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security.DataProtection;
using Ninject;
using Ninject.Syntax;
using Ninject.Web.Common;
using Web.Api.Managers;
using Web.Api.Tools;

namespace Web.Api.Configuration
{
    public static class TypeConfiguration
    {
        public static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());

            RegisterServices(kernel);

            return kernel;
        }

        private static void RegisterServices(BindingRoot kernel)
        {
            kernel.Bind<ILogger>()
                .To<Logger>()
                .InSingletonScope()
                .WithConstructorArgument("logger", NLog.LogManager.GetCurrentClassLogger());


            kernel.Bind<IUserStore<ApplicationUser>>()
                .To<ApplicationUserStore>()
                .InRequestScope();

            kernel.Bind<ApplicationDbContext>()
                .ToSelf().InRequestScope().WithConstructorArgument("connectionString", ConfigurationManager.ConnectionStrings["iBoard"].ConnectionString);

            // todo
        }
    }
}