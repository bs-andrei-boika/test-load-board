﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Web.Api.Tools
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public sealed class ValidationAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (actionContext == null)
            {
                throw new ArgumentNullException("actionContext");
            }

            List<string> errors = null;
            var modelState = actionContext.ModelState;
            var actionArgs = actionContext.ActionArguments;

            // Check for null arguments
            if (actionArgs.ContainsValue(null))
            {
                errors = new List<string> { "Request parameters are required." };
            }

            // Then check for validation attributes
            else if (!modelState.IsValid)
            {
                errors = modelState
                    .SelectMany(state => state.Value.Errors, (state, error) => error.ErrorMessage)
                    .ToList();
            }

            if (errors != null)
            {
                var errorResponse = actionContext.Request
                    .CreateResponse(HttpStatusCode.BadRequest, new { Errors = errors });

                actionContext.Response = errorResponse;
            }
        }
    }
}