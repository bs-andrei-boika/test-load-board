﻿using System.Configuration;

namespace Web.Api.Tools
{
    public static class ConfigurationTool
    {
        public static T GetSection<T>(string sectionName) where T : ConfigurationSection
        {
            return (T)ConfigurationManager.GetSection(sectionName);
        }

        public static string Get(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

        public static bool GetAsBool(string key)
        {
            bool result;
            return bool.TryParse(Get(key), out result) && result;
        }
    }
}
