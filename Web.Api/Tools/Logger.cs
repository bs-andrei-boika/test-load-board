﻿using System;

namespace Web.Api.Tools
{
    public interface ILogger
    {
        void Error(string message, params object[] args);
        void Error(Exception exception, string message = "", params object[] args);

        void Warn(string message, params object[] args);
        void Warn(Exception exception, string message = "", params object[] args);

        void Info(string message, params object[] args);
        void Info(Exception exception, string message = "", params object[] args);
    }



    public sealed class Logger : ILogger
    {
        private readonly NLog.ILogger _logger;


        public Logger(NLog.ILogger logger)
        {
            _logger = logger;
        }


        /// <summary>
        /// Writes the diagnostic message and exception at the <c>Error</c> level.
        /// </summary>
        /// <param name="exception">An exception to be logged.</param>
        /// <param name="message">A <see langword="string"/> to be written.</param>
        /// <param name="args">Arguments to format.</param>
        public void Error(Exception exception, string message = "", params object[] args)
        {
            _logger.Error(exception, message, args);
        }

        /// <summary>
        /// Writes the diagnostic message at the <c>Error</c> level using the specified parameters.
        /// </summary>
        /// <param name="message">A <see langword="string"/> containing format items.</param>
        /// <param name="args">Arguments to format.</param>
        public void Error(string message, params object[] args)
        {
            _logger.Error(message, args);
        }



        /// <summary>
        /// Writes the diagnostic message and exception at the <c>Warn</c> level.
        /// </summary>
        /// <param name="exception">An exception to be logged.</param>
        /// <param name="message">A <see langword="string"/> to be written.</param>
        /// <param name="args">Arguments to format.</param>
        public void Warn(Exception exception, string message = "", params object[] args)
        {
            _logger.Warn(exception, message, args);
        }

        /// <summary>
        /// Writes the diagnostic message at the <c>Warn</c> level using the specified parameters.
        /// </summary>
        /// <param name="message">A <see langword="string"/> containing format items.</param>
        /// <param name="args">Arguments to format.</param>
        public void Warn(string message, params object[] args)
        {
            _logger.Warn(message, args);
        }



        /// <summary>
        /// Writes the diagnostic message and exception at the <c>Info</c> level.
        /// </summary>
        /// <param name="exception">An exception to be logged.</param>
        /// <param name="message">A <see langword="string"/> to be written.</param>
        /// <param name="args">Arguments to format.</param>
        public void Info(Exception exception, string message = "", params object[] args)
        {
            _logger.Info(exception, message, args);
        }

        /// <summary>
        /// Writes the diagnostic message at the <c>Info</c> level using the specified parameters.
        /// </summary>
        /// <param name="message">A <see langword="string"/> containing format items.</param>
        /// <param name="args">Arguments to format.</param>
        public void Info(string message, params object[] args)
        {
            _logger.Info(message, args);
        }
    }
}