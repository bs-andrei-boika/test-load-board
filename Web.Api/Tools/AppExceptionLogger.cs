﻿using System.Net.Http;
using System.Text;
using System.Web.Http.ExceptionHandling;

namespace Web.Api.Tools
{
    public sealed class AppExceptionLogger : ExceptionLogger
    {
        private readonly ILogger _logger;


        public AppExceptionLogger(ILogger logger)
        {
            _logger = logger;
        }


        public override void Log(ExceptionLoggerContext context)
        {
            _logger.Error(context.Exception, RequestToString(context.Request));
        }

        private static string RequestToString(HttpRequestMessage request)
        {
            var message = new StringBuilder();

            if (request.Method != null)
            {
                message.Append(request.Method);
            }

            if (request.RequestUri != null)
            {
                message.Append(" ").Append(request.RequestUri);
            }

            return message.ToString();
        }
    }
}