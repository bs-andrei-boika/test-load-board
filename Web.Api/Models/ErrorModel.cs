﻿using System.Collections.Generic;

namespace Web.Api.Models
{
    public sealed class ErrorModel
    {
        public IEnumerable<string> Errors { get; private set; }


        public ErrorModel(string error)
        {
            Errors = new[] { error };
        }
    }
}