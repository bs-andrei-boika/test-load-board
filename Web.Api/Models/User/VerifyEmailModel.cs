﻿using System.ComponentModel.DataAnnotations;

namespace Web.Api.Models.User
{
    public sealed class VerifyEmailModel
    {
        [Required]
        public string UserId { get; set; }

        [Required]
        [RegularExpression("[0-9a-fA-F]{32}")]  // Guid without dashes
        public string Code { get; set; }
    }
}