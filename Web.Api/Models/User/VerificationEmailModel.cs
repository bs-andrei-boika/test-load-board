﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Web.Api.Models.User
{
    public sealed class VerificationEmailModel
    {
        public string UserName { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public VerificationEmailStatus Status { get; set; }
    }

    public enum VerificationEmailStatus
    {
        JustVerified,
        AlreadyVerified,
        Failed
    }
}