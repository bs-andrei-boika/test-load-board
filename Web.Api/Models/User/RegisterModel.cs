﻿using System.ComponentModel.DataAnnotations;

namespace Web.Api.Models.User
{
    public sealed class RegisterModel
    {
        [Required]
        [StringLength(300, MinimumLength = 2)]
        public string FullName { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 6)]
        public string Password { get; set; }

        [Required]
        [Compare("Password")]
        [StringLength(100, MinimumLength = 6)]
        public string ConfirmPassword { get; set; }

        [Required]
        public AccountType AccountType { get; set; }
    }


    public enum AccountType
    {
        LoadOwner,
        Carrier
    }
}