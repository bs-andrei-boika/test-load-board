﻿namespace Marmetrix.Web.Api.Models.v10.Campaign
{
    public sealed class DeleteCampaignModel
    {
        public string Id { get; set; }
    }
}