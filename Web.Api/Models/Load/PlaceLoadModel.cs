﻿using System.ComponentModel.DataAnnotations;

namespace Web.Api.Models.Load
{
    public sealed class PlaceLoadModel
    {
        [Required]
        [StringLength(100, MinimumLength = 4)]
        public string Name { get; set; }

        // todo
    }
}