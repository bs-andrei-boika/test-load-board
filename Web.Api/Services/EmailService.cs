﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace Web.Api.Services
{
    public class EmailService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            return configSendGridasync(message);
        }

        private Task<Response> configSendGridasync(IdentityMessage message)
        {

            var config = System.Configuration.ConfigurationManager.AppSettings;
            var keyValue = config["SendGridKeyValue"];
            var fromEmail = config["MailingFrom"];
            var testDestinationAcc = config["MailingTestDestinationOverrideAccount"];


            var client = new SendGridClient(keyValue);
            var from = new EmailAddress(fromEmail);
            var subject = message.Subject;
            var to = new EmailAddress(
                !string.IsNullOrEmpty(testDestinationAcc) ? testDestinationAcc : message.Destination);
            var plainTextContent = message.Body;
            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, plainTextContent);
            return client.SendEmailAsync(msg);

        }
    }
}