﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using DataAccess.Model.Data;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;
using Ninject.Web.Common.OwinHost;
using Ninject.Web.WebApi.OwinHost;
using Owin;
using Web.Api;
using Web.Api.Configuration;
using Web.Api.Managers;
using Web.Api.Security;
using Web.Api.Tools;

[assembly: OwinStartup(typeof(Startup))]

namespace Web.Api
{
    [ExcludeFromCodeCoverage]
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var httpConfiguration = new HttpConfiguration();

            // Configure routes (attribute-based)
            httpConfiguration.MapHttpAttributeRoutes();

            httpConfiguration.Routes.MapHttpRoute(
                "Error404",
                "{*url}",
                new { controller = "Error", action = "Handle404" }
            );

            // Configure content formatters: Keep only JSON formatter
            var formatters = httpConfiguration.Formatters;
            formatters.Remove(formatters.XmlFormatter);
            formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            var logger = new Logger(NLog.LogManager.GetCurrentClassLogger());

            // Configure global exception logging
            httpConfiguration.Services.Add(typeof(IExceptionLogger), new AppExceptionLogger(logger));

            // Configure CORS
            app.UseCors(CorsOptions.AllowAll);

            // Configure JWT authentication
            var authSettings = ConfigurationTool.GetSection<AuthSettings>("authSettings");
            ConfigureOAuthTokenGeneration(app, authSettings, "/oauth/token", logger);
            ConfigureOAuthTokenConsumption(app, authSettings);


            // Configure Dependency Injection (via Ninject)
            app.UseNinjectMiddleware(TypeConfiguration.CreateKernel);
            app.UseNinjectWebApi(httpConfiguration);

            httpConfiguration.EnsureInitialized();
        }


        private static void ConfigureOAuthTokenGeneration(IAppBuilder app, AuthSettings authSettings, string authPath, ILogger logger)
        {
            app.CreatePerOwinContext(() => new ApplicationUserManager(new ApplicationUserStore(new ApplicationDbContext())));

            var oAuthServerOptions = new OAuthAuthorizationServerOptions
            {
                AllowInsecureHttp = !authSettings.HttpsRequired,
                TokenEndpointPath = new PathString(authPath),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(authSettings.AccessTokenExpirationMinutes),
                Provider = new OAuthProvider(logger), // todo - pass identity
                AccessTokenFormat = new JwtWriterFormat(authSettings.JwtKey, authSettings.JwtIssuer,
                    authSettings.JwtAudience, authSettings.AccessTokenExpirationMinutes)
            };

            // OAuth 2.0 Bearer Access Token Generation
            app.UseOAuthAuthorizationServer(oAuthServerOptions);
        }


        private static void ConfigureOAuthTokenConsumption(IAppBuilder app, AuthSettings authSettings)
        {
            var key = Convert.FromBase64String(authSettings.JwtKey);

            // Api controllers with an [Authorize] attribute will be validated with JWT
            var jwtAuthOptions = new JwtBearerAuthenticationOptions
            {
                AuthenticationMode = AuthenticationMode.Active,
                AllowedAudiences = new[] { authSettings.JwtAudience },
                IssuerSecurityTokenProviders = new IIssuerSecurityTokenProvider[]
                {
                    new SymmetricKeyIssuerSecurityTokenProvider(authSettings.JwtIssuer, key)
                }
            };

            app.UseJwtBearerAuthentication(jwtAuthOptions);
        }
    }
}