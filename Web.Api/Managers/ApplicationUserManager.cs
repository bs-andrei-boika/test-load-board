﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using DataAccess.Model.Data;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.DataProtection;

namespace Web.Api.Managers
{
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> store)
            : base(store)
        {
        }

        //public ApplicationUserManager(IUserStore<ApplicationUser> store, IDataProtectionProvider dataProtectionProvider)
        //    : base(store)
        //{
        //    // Configure validation logic for usernames
        //    UserValidator = new UserValidator<ApplicationUser>(this)
        //    {
        //        AllowOnlyAlphanumericUserNames = false,
        //        RequireUniqueEmail = false
        //    };

        //    // Configure validation logic for passwords
        //    PasswordValidator = new PasswordValidator
        //    {
        //        RequiredLength = 6
        //    };

        //    // Configure user lockout defaults
        //    UserLockoutEnabledByDefault = true;
        //    DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
        //    MaxFailedAccessAttemptsBeforeLockout = 5;


        //    // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
        //    // You can write your own provider and plug it in here.
        //    //RegisterTwoFactorProvider("Phone Code", new PhoneNumberTokenProvider<ApplicationUser>
        //    //{
        //    //    MessageFormat = "Your security code is {0}"
        //    //});
        //    //RegisterTwoFactorProvider("Email Code", new EmailTokenProvider<ApplicationUser>
        //    //{
        //    //    Subject = "Security Code",
        //    //    BodyFormat = "Your security code is {0}"
        //    //});
        //    //EmailService = new EmailService();
        //    //SmsService = new SmsService();
        //    if (dataProtectionProvider != null)
        //    {
        //        UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(dataProtectionProvider.Create("ASP.NET Identity"))
        //        {
        //            TokenLifespan = TimeSpan.FromHours(1)
        //        };
        //    }
        //}
    }
}