﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAccess.Model.Data;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Web.Api.Managers
{
    public class ApplicationUserStore : UserStore<ApplicationUser>
    {
        public ApplicationUserStore(ApplicationDbContext context)
            : base(context)
        {
        }
    }
}