﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using DataAccess.Model.Data;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace Web.Api.Managers
{
    public class ApplicationSignInManager : SignInManager<ApplicationUser, string>
    {
        public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override async Task<ClaimsIdentity> CreateUserIdentityAsync(ApplicationUser user)
        {
            ClaimsIdentity identity = await user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
            return identity;
        }
    }
}