﻿namespace Web.Api.Security
{
    public static class Roles
    {
        public const string LoadOwner = "LoadOwner";

        public const string Carrier = "Carrier";


        public static readonly string[] AllRoles = { LoadOwner, Carrier };
    }
}