﻿using System;
using System.IdentityModel.Tokens;
using Microsoft.Owin.Security;

namespace Web.Api.Security
{
    public sealed class JwtWriterFormat : ISecureDataFormat<AuthenticationTicket>
    {
        private const string SignatureAlgorithm = "http://www.w3.org/2001/04/xmldsig-more#hmac-sha256";
        private const string DigestAlgorithm = "http://www.w3.org/2001/04/xmlenc#sha256";

        private readonly string _jwtKey;
        private readonly string _jwtIssuer;
        private readonly string _jwtAudience;
        private readonly int _accessTokenExpirationMinutes;


        public JwtWriterFormat(string jwtKey, string jwtIssuer, string jwtAudience, int accessTokenExpirationMinutes)
        {
            _jwtKey = jwtKey;
            _jwtIssuer = jwtIssuer;
            _jwtAudience = jwtAudience;
            _accessTokenExpirationMinutes = accessTokenExpirationMinutes;
        }


        public string Protect(AuthenticationTicket data)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }

            var key = Convert.FromBase64String(_jwtKey);
            var now = DateTime.UtcNow;
            var expires = now.AddMinutes(_accessTokenExpirationMinutes);

            var signingCredentials = new SigningCredentials(new InMemorySymmetricSecurityKey(key),
                SignatureAlgorithm, DigestAlgorithm);

            var token = new JwtSecurityToken(_jwtIssuer, _jwtAudience, data.Identity.Claims,
                now, expires, signingCredentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public AuthenticationTicket Unprotect(string protectedText)
        {
            throw new NotImplementedException();
        }
    }
}