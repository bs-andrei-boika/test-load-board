﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Web.Api.Managers;
using Web.Api.Tools;
using Microsoft.AspNet.Identity.Owin;

namespace Web.Api.Security
{
    public sealed class OAuthProvider : OAuthAuthorizationServerProvider
    {
        private readonly ILogger _logger;


        public OAuthProvider(ILogger logger)
        {
            _logger = logger;
        }


        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            context.Validated();
            return Task.FromResult(0);
        }


        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            try
            {
                var userManager = context.OwinContext.Get<ApplicationUserManager>();

                var user = await userManager.FindByEmailAsync(context.UserName);
                if (await userManager.CheckPasswordAsync(user, context.Password))
                {
                    var identity = new ClaimsIdentity("JWT");
                    identity.AddClaim(new Claim(ClaimTypes.Name, context.UserName));
                    identity.AddClaim(new Claim("sub", context.UserName));

                    context.Validated(new AuthenticationTicket(identity, new AuthenticationProperties()));
                }
                else
                {
                    context.SetError("invalid_grant", "The username or password is incorrect, or your email address is not verified.");
                }
            }
            catch (Exception exception)
            {
                _logger.Error(exception);
                context.SetError("server_error", "Server-side application error.");
            }
        }


        public override Task GrantRefreshToken(OAuthGrantRefreshTokenContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            var oldTicket = context.Ticket;

            var identity = new ClaimsIdentity(oldTicket.Identity);
            var ticket = new AuthenticationTicket(identity, oldTicket.Properties);

            context.Validated(ticket);
            return Task.FromResult(0);
        }
    }
}