(function (module) {
    'use strict';

    var oauth = function (constants) {

        var url = constants.WEBAPI_HOST + "/oauth/token";

        this.$get = ["$http", "formEncode", "currentUser", function($http, formEncode, currentUser) {

            var requestConfig = {
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            };

            var processToken = function(username) {
                return function(response) {
                    currentUser.profile.username = username;
                    currentUser.profile.token = response.data.access_token;
                    currentUser.save();
                    return username;
                };
            };

            var login = function(username, password) {

                var data = formEncode({
                    username: username,
                    password: password,
                    grant_type: "password"
                });

                return $http
                    .post(url, data, requestConfig)
                    .then(processToken(username));
            };

            var logout = function() {
                currentUser.profile.username = "";
                currentUser.profile.token = "";
                currentUser.remove();
            };

            return {
                login: login,
                logout: logout
            };
        }];
    };

    module.config(['$provide', function ($provide) {
        $provide.provider("oauth", ['constants', oauth]);
    }]);

}(angular.module('app.common')));