(function(module) {
    'use strict';

    var loginRedirect = function() {

        var loginUrl = "/login";

        this.$get = ["$q", "$location", function($q, $location) {

            return {
                responseError: function(response) {
                    // Request is not authenticated/authorized (User is not logged in or token is expired)
                    if (response.status === 401 || response.status === 403) {
                        $location.path(loginUrl);
                    }

                    return $q.reject(response);
                }
            };
        }];
    };


    module
        .provider("loginRedirect", [
            '$httpProvider',
            loginRedirect
        ])
        .config([
            '$httpProvider', function($httpProvider) {
                $httpProvider.interceptors.push('loginRedirect');
            }
        ]);

}(angular.module("app.common")));