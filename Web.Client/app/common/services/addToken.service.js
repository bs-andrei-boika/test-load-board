(function(module) {
    'use strict';

    var addToken = function(currentUser, $q) {

        return {
            request: function (config) {

                config.headers = config.headers || {};

                if (currentUser.profile.token) {
                    config.headers.Authorization = "Bearer " + currentUser.profile.token;
                }

                return $q.when(config);
            }
        };
    };

    module
        .factory("addToken", [
            "currentUser",
            "$q",
            addToken
        ])
        .config(
            ['$httpProvider',
                function ($httpProvider) {
                    $httpProvider.interceptors.push("addToken");
        }]);

})(angular.module("app.common"));