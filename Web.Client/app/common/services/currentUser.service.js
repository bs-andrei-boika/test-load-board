(function(module) {
    'use strict';

    var USERKEY = 'utoken';

    var service = function (localStorage) {

        var initialize = function() {
            var user = {
                username: '',
                token: ''
            };

            var localUser = localStorage.get(USERKEY);
            if (localUser) {
                user.username = localUser.username;
                user.token = localUser.token;
            }
            return user;
        };

        var profile = initialize();


        return {
            save: function () {
                localStorage.add(USERKEY, profile);
            },

            remove: function () {
                localStorage.remove(USERKEY);
            },

            profile: profile
        };
    };

    module.factory('currentUser', [
        "localStorage",
        service
    ]);

}(angular.module('app.common')));