(function (module) {
    'use strict';

    module.constant('constants', {
        WEBAPI_HOST: 'http://localhost:8500',
        WEBAPI_PATH: '/api/v1.0'
    });

}(angular.module('app.common')));