﻿(function (module) {
    'use strict';

    var filter = function($sce) {
        return function (input, searchParam) {
            if (typeof input === 'function') {
                return '';
            }
            if (searchParam) {
                var words = '(' +
                      searchParam.split(/\ /).join(' |') + '|' +
                      searchParam.split(/\ /).join('|') +
                    ')',
                    exp = new RegExp(words, 'gi');
                if (words.length) {
                    input = input.replace(exp, "<span class=\"highlight\">$1</span>");
                }
            }
            return $sce.trustAsHtml(input);
        };
    };

    module.filter("highlight", ['$sce', filter]);

}(angular.module("app.common")));