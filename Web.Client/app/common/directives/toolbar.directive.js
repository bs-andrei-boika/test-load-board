(function (module) {
    'use strict';

    var directive = function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                $(element).toolbar(scope.$eval(attrs.toolbarTip));
            }
        };
    };
    module.directive("toolbarTip", directive);
}(angular.module("app.common")));
