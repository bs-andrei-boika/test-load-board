(function (module) {
    'use strict';

    var directive = function ($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element) {
                $timeout(function () {
                    element.slimscroll({
                        height: '100%',
                        railOpacity: 0.9
                    });

                },200);
            }
        };
    };
    module.directive("fullScroll", ['$timeout',directive]);
}(angular.module("app.common")));
