(function(module) {
    'use strict';

    module
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            function($stateProvider, $urlRouterProvider) {
                $stateProvider
                    .state('main', {
                        url: '/main',
                        templateUrl: 'app/main/views/main.view.html',
                        controller: 'mainController',
                        controllerAs: 'main'
                    })
                    .state('main.loadOwner', {
                        url: '/owner',
                        templateUrl: 'app/main/views/loadOwner.view.html',
                        controller: 'accountController',
                        controllerAs: 'account'
                    })
                    .state('main.carrier', {
                        url: '/carrier',
                        templateUrl: 'app/main/views/carrier.view.html',
                        controller: 'campaignController',
                        controllerAs: 'campaign'
                    })
                    .state('login', {
                        url: '/login',
                        templateUrl: 'app/security/views/login.view.html',
                        controller: 'loginController',
                        controllerAs: 'login'
                    })
                    .state('register', {
                        url: '/register',
                        templateUrl: 'app/security/views/register.view.html',
                        controller: 'registerController',
                        controllerAs: 'user'
                    })
                    .state('verify', {
                        url: '/verify/:userId/{code:[0-9a-fA-F]{32}}',
                        templateUrl: 'app/security/views/verify.view.html',
                        controller: 'verifyController',
                        controllerAs: 'verify'
                    });

                $urlRouterProvider.otherwise('/main');
            }
        ])
        .config(["$sceDelegateProvider", function($sceDelegateProvider) {
            $sceDelegateProvider.resourceUrlWhitelist([
                'self',
                'http://localhost:8500/**'
            ]);
        }])
        .run(["$rootScope","$state",function($rootScope, $state) {
            $rootScope.$state = $state;
        }]);

}(angular.module('app')));