(function(module) {
    'use strict';

    var controller = function ($state, $scope, sweet, currentUser, dbUser) {

        if (!!currentUser.profile.token) {
            $state.go('main');
            return;
        }

        $scope.hasError = function (field, rule) {
            if (!$scope.register) {
                return false;
            }
            var input = $scope.register[field];
            return (!!rule ? input.$error[rule] : input.$invalid) && (input.$dirty || !!$scope.submitted);
        };

        var vm = this;
        vm.adding = false;
        vm.item = {
            fullName: '',
            emailAddress: '',
            accountType: '',
            password: '',
            confirmPassword: ''
        };

        vm.keyPressed = function (event) {
            if (event.which === 27) {
                vm.clickedCancel();
            }
        };

        vm.clickedSubmit = function () {
            console.log('Creating user record for ' + vm.item.login);

            $scope.submitted = true;
            if (!$scope.register.$valid) {
                return;
            }

            vm.adding = true;

            dbUser.create(vm.item, function (success, response) {
                if (success) {
                    sweet.swal({
                        title: 'User "' + vm.item.login + '" created',
                        html: true,
                        text: 'Verification email was send to your <b>"' + vm.item.emailAddress + '"</b> email address. Please, check it and follow the instructions.',
                        type: 'success',
                        confirmButtonColor: '#1ab394',
                        closeOnConfirm: true
                    },
                    function() {
                        $state.go('login');
                    });
                } else {
                    var error = (response.data.errors || [''])[0];

                    switch (error) {
                        case 'DuplicateUserName':
                            error = 'User with login <b>"' + vm.item.login + '"</b> already exists.';
                            break;
                        case 'DuplicateEmail':
                            error = 'User with email address <b>"' + vm.item.emailAddress + '"</b> already exists.';
                            break;
                        default:
                            error = error || 'User was not created.';
                    }

                    sweet.swal({
                        title: 'Oops, error',
                        html: true,
                        text: error,
                        type: 'error',
                        confirmButtonColor: '#dd6b55',
                        closeOnConfirm: true
                    });
                }
                vm.adding = false;
            });
        };

        vm.clickedCancel = function() {
            $state.go('login');
        };

    };

    module.controller('registerController', [
        '$state',
        '$scope',
        'SweetAlert',
        'currentUser',
        'userService',
        controller
    ]);

}(angular.module('app.security')));