(function (module) {
    'use strict';

    var loginController = function ($scope, $state, oauth, currentUser, $location, sweet) {

        if (!!currentUser.profile.token) {
            $state.go('main');
        }

        $scope.hasError = function (field, rule) {
            var input = $scope.loginForm[field];
            return (!!rule ? input.$error[rule] : input.$invalid) && (input.$dirty || !!$scope.submitted);
        };

        var model = this;

        model.loggingIn = false;
        model.email = '';
        model.password = '';
        model.failedToLogin = false;
        model.user = currentUser.profile;
        model.login = function () {
            $scope.submitted = true;
            if (!$scope.loginForm.$valid) {
                return;
            }

            model.loggingIn = true;
            oauth.login(model.email, model.password)
                .then(function() {
                    $location.path("/");
                    model.loggingIn = false;
                })
                .catch(function(response) {
                    model.password = '';
                    model.loggingIn = false;
                    sweet.swal({
                        title: 'Login error',
                        html: true,
                        text: response.data.error_description !== undefined && response.data.error_description || response.data.message,
                        type: 'error',
                        confirmButtonColor: '#dd6b55',
                        closeOnConfirm: true
                    });
                });
        };

        model.signOut = function () {
            sweet.swal({
                title: 'Are you sure?',
                text: 'You are going to logout from application.',
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: 'No',
                confirmButtonColor: '#dd6b55',
                confirmButtonText: 'Yes, logout',
                closeOnConfirm: true,
                closeOnCancel: true
            }, function (result) {
                if (result) {
                    oauth.logOut();
                }
            });
        };
    };

    module.controller('loginController', [
        '$scope',
        '$state',
        'oauth',
        'currentUser',
        '$location',
        'SweetAlert',
        loginController
    ]);

}(angular.module('app.security')));