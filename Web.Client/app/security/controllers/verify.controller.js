﻿(function (module) {
    'use strict';

    var controller = function ($state, $stateParams, sweet, dbUser, spinners) {
        var vm = this;

        vm.init = function () {
            spinners.show('verifySpinner');

            var code = $stateParams.code,
                userId = $stateParams.userId;

            console.log('Verifying user "' + userId + '" by code ' + code);

            dbUser.verify(userId, code, function(success, response) {
                var options = {
                    html: true,
                    closeOnConfirm: true,
                    confirmButtonColor: '#1ab394',
                    title: 'Email verification'
                };

                if (success) {
                    if (response.status === 'JustVerified') {
                        // User has been verified right now
                        options.text = 'User <b>"' + response.userName + '"</b> successfully verified.<br/>Please, log in to the application.';
                        options.type = 'success';
                    } else {
                        // User has been already verified earlier
                        options.text = 'User <b>"' + response.userName + '"</b> is already verified.<br/>Please, log in to the applciation.';
                        options.type = 'info';
                    }
                } else {
                    // Error occurs (hide all details)
                    options.title = 'Verification error';
                    options.text = 'Email was not verified. Your link is not valid.';
                    options.type = 'error';
                    options.confirmButtonColor = '#dd6b55';
                }

                spinners.hide('verifySpinner');

                sweet.swal(options, function() {
                    $state.go('login');
                });
            });
        };
    };

    module.controller('verifyController', [
        '$state',
        '$stateParams',
        'SweetAlert',
        'userService',
        'spinnerService',
        controller
    ]);

}(angular.module('app.security')));