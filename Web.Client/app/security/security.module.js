(function () {
    'use strict';

    angular
        .module('app.security',
        [
            'ngAnimate',
            'ui.bootstrap',
            'app.common'
        ]);
})();