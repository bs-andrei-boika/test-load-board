(function() {
    'use strict';

    angular
        .module('app', [
            'ui.router',
            'ui.mask',
            'app.main',
            'app.common',
            'app.security',
            'ngSanitize',
            'ui.bootstrap'
        ]);
})();