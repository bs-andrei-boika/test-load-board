(function (module) {
    'use strict';

    var controller = function (dbLoad) {

        var vm = this;
        vm.isLoading = true;
        vm.model = {
            // todo
        };


        vm.initForLoadOwner = function () {
            vm.isLoading = true;
            dbLoad.get(function(success, data) {
                if (success) {
                    // todo
                    vm.isLoading = false;
                }
            });
        };

        vm.initForCarrier = function() {
            vm.isLoading = true;
            dbLoad.check(function (success, data) {
                if (success) {
                    // todo
                    vm.isLoading = false;
                }
            });
        };

        vm.placeLoad = function() {
            // todo POST vm.model
        };
    };

    module
        .controller('loadController', [
            'loadService',
            controller
        ]);

})(angular.module('app.main'));