(function (module) {
    'use strict';

    var controller = function (currentUser, $state) {
        console.log('mainController..');

        var vm = this;

        // todo

        vm.init = function() {
            if (!currentUser.profile.token) {
                $state.go('login');
            }
        };

    };

    module
        .controller('mainController', [
            'currentUser',
            '$state',
            controller
        ]);

})(angular.module('app.main'));