﻿(function(module) {
    'use strict';

    var service = function ($resource, constants) {

        var url = constants.WEBAPI_HOST + constants.WEBAPI_PATH,
            resource = $resource(url + '/user', { }, {
                carriers: {
                    method: 'GET',
                    url: url + '/user/carriers'
                },
                verify: {
                    method: 'POST',
                    url: url + '/user/verify'
                }
            });


        function register(user, callback) {
            resource.save({
                    fullName: user.fullName,
                    email: user.emailAddress,
                    password: user.password,
                    confirmPassword: user.confirmPassword,
                    accountType: user.accountType
                },
                function(response) {
                    if (!!callback) {
                        callback(true, response);
                    }
                },
                function(response) {
                    if (!!callback) {
                        callback(false, response);
                    }
                });
        }


        function verify(userId, code, callback) {
            resource.verify({
                    userId: userId,
                    code: code
                },
                function(response) {
                    if (!!callback) {
                        callback(true, response);
                    }
                },
                function(response) {
                    if (!!callback) {
                        callback(false, response);
                    }
                });
        }

        function getCarriers() {
            resource.carriers(
                function (response) {
                    if (!!callback) {
                        callback(true, response);
                    }
                },
                function (response) {
                    if (!!callback) {
                        callback(false, response);
                    }
                });
        }



        return {
            create: register,
            verify: verify,
            getCarriers: getCarriers
        };
    };

    module
        .factory("userService", [
            '$resource',
            'constants',
            service
        ]);

})(angular.module("app.main"));