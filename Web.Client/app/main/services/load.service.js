﻿(function(module) {
    'use strict';

    var service = function ($resource, constants) {

        var url = constants.WEBAPI_HOST + constants.WEBAPI_PATH,
            resource = $resource(url + '/loads', {}, {
                check: {
                    method: 'GET',
                    url: url + '/campaigns/check'
                }
            });


        function getLoads(callback) {
            resource.query(
                function(response) {
                    if (!!callback) {
                        callback(true, response);
                    }
                },
                function(response) {
                    if (!!callback) {
                        callback(false, response);
                    }
                });
        }


        function checkLoads(callback) {
            resource.check(
                function (response) {
                    if (!!callback) {
                        callback(true, response);
                    }
                },
                function (response) {
                    if (!!callback) {
                        callback(false, response);
                    }
                });
        }


        function placeLoad(item, callback) {
            resource.save({
                    name: item.name,
                    active: item.active,
                    defaultUrl: item.defaultUrl,
                    marginOfError: item.marginOfError
                },
                function(response) {
                    getLoads(function (success) {
                        if (!!callback) {
                            callback(success, response.id);
                        }
                    });
                },
                function(response) {
                    if (!!callback) {
                        callback(false, response);
                    }
                });
        }



        return {
            get: function(callback) {
                getLoads(callback);
            },

            check: function (callback) {
                checkLoads(callback);
            },

            place: function(item, callback) {
                return placeLoad(item, callback);
            }
        };
    };

    module
        .factory("loadService", [
            '$resource',
            'constants',
            service
        ]);

})(angular.module("app.main"));