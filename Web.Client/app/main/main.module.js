(function () {
    'use strict';

    angular
        .module('app.main',
        [
            'ngAnimate',
            'ui.bootstrap',
            'NgSwitchery',
            'angularBootstrapNavTree',
            'oitozero.ngSweetAlert',
            'ui.slider',
            'ngResource'
        ]);
})();