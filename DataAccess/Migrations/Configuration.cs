using DataAccess.Model.Data;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DataAccess.Model.Data.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(DataAccess.Model.Data.ApplicationDbContext context)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            var loadOwnerRole = new IdentityRole { Name = "LoadOwner" };
            var carrierRole = new IdentityRole { Name = "Carrier" };

            roleManager.Create(loadOwnerRole);
            roleManager.Create(carrierRole);

            var loadOwner = new ApplicationUser { UserName = "LoadOwner", Email = "LoadOwner@mail.com", FullName = "First Load Owner" };

            var password = "123321";
            var result = userManager.Create(loadOwner, password);

            if (result.Succeeded) userManager.AddToRole(loadOwner.Id, loadOwnerRole.Name);


            var carrier = new ApplicationUser { UserName = "Carrier", Email = "Carrier@mail.com", FullName = "First Carrier" };
            password = "12344321";
            result = userManager.Create(carrier, password);
            if (result.Succeeded) userManager.AddToRole(carrier.Id, carrierRole.Name);

            base.Seed(context);
        }
    }
}
