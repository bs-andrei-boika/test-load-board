﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DataAccess.Model.Data
{
    public class ApplicationUser : IdentityUser
    {
        public virtual async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);

            return userIdentity;
        }

        public string FullName { get; set; }

        public virtual ICollection<Load> OwnerLoads { get; set; }

        public virtual ICollection<Load> CarrierLoads { get; set; }
    }
}
