﻿using System.Configuration;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DataAccess.Model.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base(ConfigurationManager.ConnectionStrings["iBoard"].ConnectionString, false)
        { }

        public ApplicationDbContext(string connectionString)
            : base(connectionString, false)
        { }

        public virtual DbSet<Load> Loads { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);


            modelBuilder.Entity<IdentityUserRole>()
            .HasKey(r => new { r.UserId, r.RoleId })
            .ToTable("AspNetUserRoles");

            modelBuilder.Entity<IdentityUserLogin>()
                .HasKey(l => new { l.LoginProvider, l.ProviderKey, l.UserId })
                .ToTable("AspNetUserLogins");


            modelBuilder.Entity<Load>()
                    .HasRequired<ApplicationUser>(l => l.Owner)
                    .WithMany(s => s.OwnerLoads)
                    .HasForeignKey(s => s.OwnerId);

            modelBuilder.Entity<Load>()
                    .HasOptional<ApplicationUser>(l => l.Carrier)
                    .WithMany(s => s.CarrierLoads)
                    .HasForeignKey(s => s.CarrierId);

            modelBuilder.Entity<Load>().Property(x => x.ZipcodeFrom).HasMaxLength(10);
            modelBuilder.Entity<Load>().Property(x => x.ZipcodeTo).HasMaxLength(10);

        }
    }
}
