﻿namespace DataAccess.Model.Data
{
    public class Load
    {
        public int Id { get; set; }
        public string Commodity { get; set; }
        public string Weight { get; set; }
        public string AddressFrom { get; set; }
        public string ZipcodeFrom { get; set; }
        public string AddressTo { get; set; }
        public string ZipcodeTo { get; set; }


        public virtual string OwnerId { get; set; }
        public virtual ApplicationUser Owner { get; set; }

        public virtual string CarrierId { get; set; }
        public virtual ApplicationUser Carrier { get; set; }
    }
}
